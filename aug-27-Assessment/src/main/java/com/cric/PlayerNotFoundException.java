package com.cric;

public class PlayerNotFoundException extends Exception{
    public PlayerNotFoundException(String msg)
    {
        super(msg);
    }
}
