package com.cric;

import java.util.*;
import java.util.regex.PatternSyntaxException;

class Details{
    int id;
    String name;
    int matches;
    int runs;
    int wickets;
    int outZero;
    String playerType;

    public Details()
    {

    }
    public Details(int id, String name, int matches, int runs, int wickets, int outZero, String playerType)
    {
        this.id = id;
        this.name = name;
        this.matches = matches;
        this.runs = runs;
        this.wickets = wickets;
        this.outZero = outZero;
        this.playerType = playerType;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMatches() {
        return matches;
    }

    public void setMatches(int matches) {
        this.matches = matches;
    }

    public int getRuns() {
        return runs;
    }

    public void setRuns(int runs) {
        this.runs = runs;
    }

    public int getWickets() {
        return wickets;
    }

    public void setWickets(int wickets) {
        this.wickets = wickets;
    }

    public int getOutZero() {
        return outZero;
    }

    public void setOutZero(int outZero) {
        this.outZero = outZero;
    }

    public String getPlayerType() {
        return playerType;
    }

    public void setPlayerType(String playerType) {
        this.playerType = playerType;
    }

}

class playerDetails
{
    public void allPlayersSorting(List<Details> data) {
        Collections.sort(data, new ScoreComparator());
        System.out.println("ID Name Matches_Played Total_Runs_Scored Wickets_Taken Out_Zero_Score Player_Type ");
        Iterator<Details> itr1 = data.iterator();
        while (itr1.hasNext()) {
            Details cd = itr1.next();
            System.out.println(cd.getId() + " " + cd.getName() + " " + cd.getMatches() + " " + cd.getRuns() + " " + cd.getWickets() + " " + cd.getOutZero() + " " + cd.getPlayerType());
        }
    }

    public void finalPlayersSorting(List<Details> data)
    {
        Collections.sort(data, new ScoreComparator());
        ArrayList<Details> selected_list = new ArrayList<>(11);
        int count =0;
        for(Details s:data){
            if(count==11){
                break;
            }
            selected_list.add(s);
            count++;
        }
        Collections.sort(selected_list,new NameComparator());
        System.out.println("Final selected players are: ");
        System.out.println("ID Name Matches_Played Total_Runs_Scored Wickets_Taken Out_Zero_Score Player_Type ");
        Iterator<Details> itr2 = selected_list.iterator();
        while(itr2.hasNext())
        {
            Details cd = itr2.next();
            System.out.println(cd.getId() + " " + cd.getName() + " " + cd.getMatches() + " " + cd.getRuns() + " " + cd.getWickets()+ " "+cd.getOutZero()+" " + cd.getPlayerType());
        }
    }

    public void updatePlayerInfoByName(Map<String, Details> info) throws PlayerNotFoundException {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter player name to be updated: ");
        String name = sc.nextLine();

        Details playerName = info.get(name);
        if(playerName == null){
            PlayerNotFoundException pne = new PlayerNotFoundException("Player with given name not found.");
            throw (pne);
        }

        info.remove(name);

        System.out.println("Enter updated id of the player: ");
        playerName.setId(sc.nextInt());
        sc.nextLine();
        System.out.println("Enter the updated player name: ");
        playerName.setName(sc.nextLine());
        System.out.println("Enter the updated number of matches played: ");
        playerName.setMatches(sc.nextInt());
        System.out.println("Enter the updated number of runs scored: ");
        playerName.setRuns(sc.nextInt());
        System.out.println("Enter the updated number of wickets taken: ");
        playerName.setWickets(sc.nextInt());
        System.out.println("Enter updated number of outs at zero runs: ");
        playerName.setOutZero(sc.nextInt());
        System.out.println("Enter updated type of player-  1: Bowler , 2: Batsman , 3:Wicket-Keeper , 4: All-Rounder");
        switch (sc.nextInt()){
            case 1:
                playerName.setPlayerType("Bowler");
                break;
            case 2:
                playerName.setPlayerType("Batsman");
                break;
            case 3:
                playerName.setPlayerType("Wicket-Keeper");
                break;
            case 4:
                playerName.setPlayerType("All-Rounder");
                break;
            default:
                System.out.println("Player is set as batsman.");
                playerName.setPlayerType("Batsman");
                break;
        }

        info.put(playerName.getName(),playerName);
    }

    public void setGetDetails()
    {
        List<Details> data = new ArrayList<Details>();
        Map<String,Details> info = new HashMap<>();
        Details player1 = new Details(1,"Shikhar Dhawan",5,250,7,2,"Batsman");
        Details player2 = new Details(2,"Virat Kohli",8,500,4,3,"Batsman");
        Details player3 = new Details(3,"Ravindra Jadeja",9,750,9,4,"All-Rounder");
        Details player4 = new Details(4,"Mohammed Shami",5,20,10,2,"Bowler");
        Details player5 = new Details(5,"Manish Pandey",8,650,4,3,"Bowler");
        data.add(player1);
        data.add(player2);
        data.add(player3);
        data.add(player4);
        data.add(player5);
        info.put(player1.getName(),player1);
        info.put(player2.getName(),player2);
        info.put(player3.getName(),player3);
        info.put(player4.getName(),player4);
        info.put(player5.getName(),player5);
    }
}

class ScoreComparator implements Comparator<Details>
{
    @Override
    public int compare(Details details, Details t1) {
        int avg1 = (details.getRuns()/details.getMatches());
        int avg2=(t1.getRuns()/ t1.getMatches());
        return avg2-avg1;
    }
}
class NameComparator implements Comparator<Details>
{
    @Override
    public int compare(Details o1, Details o2) {
        return o1.getName().compareTo(o2.getName());
    }
}

public class CricTeam {
    public static void main(String[] args) throws PlayerNotFoundException {
        Scanner sc= new Scanner(System.in);
        List<Details> data = new ArrayList<Details>();
        Map<String,Details> info = new HashMap<>();
        playerDetails pd = new playerDetails();
        pd.setGetDetails();
        int i;
        while(true){
            System.out.println(" 1: Display All The Players \n 2: Update The Player Information By Name \n 3: Display Final Team \n 4: Exit");
            System.out.println("Enter your choice : ");
            i= sc.nextInt();
            switch (i){
                case 1:
                    pd.allPlayersSorting(data);
                    break;
                case 2:
                    pd.updatePlayerInfoByName(info);
                    break;
                case 3:
                    pd.finalPlayersSorting(data);
                    break;
                case 4:
                    System.exit(0);
                    break;
                default:
                    System.out.println("Invalid choice. Please try again.");
                    break;
            }
        }

    }
}
